# Python Incolume Utils

--------

_Projeto desenvolvido e administrado incolume.com.br_

---

Este pacote disponibiliza diversas funcionalidades para utilização em rotinas com Python.

Dentre elas:

* geração de números de CPF não verificados;

* geração de lista para namespace para pacotes Python;

* obtenção rápida para conteúdo de arquivos;

* sequências de vários tipos;

* Nome único para gravação de arquivos sem sobrescrita;


## Instalar o pacote

```shell
pip install incolumepy.utils
```
```shell
pipenv install incolumepy.utils
```

```shell
poetry add incolumepy.utils
```

```shell
poetry add git+https://gitlab.com/development-incolume/incolumepy.utils.git#main
```

## Atualizar o pacote
```shell

pip install -U incolumepy.utils
```
```shell
pipenv update incolumepy.utils
```
```shell
poetry update incolumepy.utils
```

```shell
poetry update git+https://gitlab.com/development-incolume/incolumepy.utils.git#main
```

## Gerar pacote a partir dos fontes para instalação
```shell
python setup.py bdist_egg bdist_wheel
```
```shell
poetry build
```

## Exemplos
Exemplos disponíveis em [docs/EXAMPLES]('docs/EXAMPLES.rst')

