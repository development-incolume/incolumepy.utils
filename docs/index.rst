.. Incolume Python Lex documentation master file, created by
   sphinx-quickstart on Tue Feb  8 10:55:41 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentação para Incolume Python Utils!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Contents
--------

.. toctree::
    prefacio
    usage
    api
    CONTRIBUTORS
    CHANGESLOG
    development
    COMMITS
    EXAMPLES
